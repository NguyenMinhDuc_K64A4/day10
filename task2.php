<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    .main{
        padding-top:20px;
        margin-left: auto;
        margin-right: auto;
        width: 800px;
        height: auto;
        background-color: #3984bc;
    }
    .question_1{

    }
    .question{
        font-size: 20px;
        font-weight: bold;
        text-align: center;
        padding-bottom: 10px;
    }
    .list-answer li {
        font-size: 15px;
        padding-left: 50px;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .cut{
        width: 100%;
        height: 3px;
        background-color: black;
    }
    .btn-next button{
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 16px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
    }

    .btn-next button:hover {
        background-color: #4CAF50;
        color: white;
    }
    </style>
</head>
<body>
    <?php
        session_start();
    
        $listQA6 = array("1","2","3","4");
        $listQA7 = array("lúa","ngô","khoai","sắn");
        $listQA8 = array("đông","tây","nam","bắc");
        $listQA9 = array("facebook","telegram","tiktok","instagram");
        $listQA10 = array("1","2","3","4");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            
            if(isset($_POST["answer6"])&&isset($_POST["answer7"])&&isset($_POST["answer8"])&&isset($_POST["answer9"])&&isset($_POST["answer10"])){
                $answer6 = $_POST["answer6"];
                $qa6 = "1 + 2 = ?";
                $answer7 = $_POST["answer7"];
                $qa7 = "đâu là cây lương thực nhiều nhất thế giới ?";
                $answer8 = $_POST["answer8"];
                $qa8 = "hướng nào là hướng mặt trời mọc?";
                $answer9 = $_POST["answer9"];
                $qa9 = "đâu mạng xã hội lớn thứ hai thế giới?";
                $answer10 = $_POST["answer10"];
                $qa10 = "Có bao nhiêu chữ C trong câu sau: Cơm, canh, cá, tất cả em đều thích? ";
                setcookie("answer6",$answer6, time() + (86400 * 30), "/");
                setcookie("qa6",$qa6, time() + (86400 * 30), "/");
                setcookie("answer7",$answer7, time() + (86400 * 30), "/");
                setcookie("qa7",$qa7, time() + (86400 * 30), "/");
                setcookie("answer8",$answer8, time() + (86400 * 30), "/");
                setcookie("qa8",$qa8, time() + (86400 * 30), "/");
                setcookie("answer9",$answer9, time() + (86400 * 30), "/");
                setcookie("qa9",$qa9, time() + (86400 * 30), "/");
                setcookie("answer10",$answer10, time() + (86400 * 30), "/");
                setcookie("qa10",$qa10, time() + (86400 * 30), "/");
                header("location: checking.php");
            }else{
                echo "<div style='color: red;font-size: 20px;font-weight: bold;text-align: center;'>Hãy nhập trả lời tất cả các câu hỏi</div>";
            }

        }
        
    ?>
            <form name="registerForm" method="post" enctype="multipart/form-data" action="">
                <div class="main">
                    <div class="question_6">
                        <div class="question"> 1 + 2 = ?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA6 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer6'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_7">
                        <div class="question">đâu là cây lương thực nhiều nhất thế giới ?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA7 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer7'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_8">
                        <div class="question">hướng nào là hướng mặt trời mọc?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA8 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer8'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_9">
                        <div class="question">đâu mạng xã hội lớn thứ hai thế giới?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA9 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer9'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="question_10">
                        <div class="question">Có bao nhiêu chữ C trong câu sau: "Cơm, canh, cá, tất cả em đều thích"?</div>
                        <ul class="list-answer" style="list-style-type:none;">
                            <?php
                                foreach($listQA10 as $x){
                                    echo"<li>
                                            <input class='tick-question' type='radio' value='$x' name='answer10'>"
                                            .$x.
                                        "</li>";
                                }
                            ?>    
                        </ul>  
                        <div class="cut"></div>
                    </div>
                    <div class="btn-next">
                        <button >
                            Nộp Bài
                        </button>
                    </div>  
                </div>
            </form>


</body>
</html>
